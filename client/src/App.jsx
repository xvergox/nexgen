import React, { useState, useEffect } from 'react';
import axios from 'axios';

function UserList() {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(true);
  const [newUser, setNewUser] = useState({ id: '', name: '', age: '' });

  useEffect(() => {
    async function fetchUsers() {
      try {
        const response = await axios.get('http://localhost:29/api/users');
        if (response.status === 200) {
          setUsers(response.data);
        } else {
          console.error('Failed to fetch users:', response.status);
        }
      } catch (error) {
        console.error('Error fetching users:', error);
      } finally {
        setLoading(false);
      }
    }

    fetchUsers();
  }, []);

  const handleInputChange = (event) => {
    
    const { name, value } = event.target;
    setNewUser((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  
  const handleDeleteUser = async (userId) => {
    try {
      const response = await axios.delete(`http://localhost:29/api/users/${userId}`);
      if (response.status === 200) {
        // Обновляем список пользователей, убирая удаленного пользователя
        setUsers((prevUsers) => prevUsers.filter((user) => user.id !== userId));
      } else {
        console.error('Failed to delete user:', response.status);
      }
    } catch (error) {
      console.error('Error deleting user:', error);
    }
  };
  const handleAddUser = async () => {
    try {
      const response = await axios.post('http://localhost:29/api/users:add', newUser);
      if (response.status === 200) {
        // Обновляем список пользователей
        setUsers((prevUsers) => [...prevUsers, newUser]);
        // Очищаем данные нового пользователя
        setNewUser({ id: '', name: '', age: '' });
      } else {
        console.error('Failed to add user:', response.status);
      }
    } catch (error) {
      console.error('Error adding user:', error);
    }
  };




  return (
    <div>
      <h2>User List</h2>
      {loading ? (
        <div>Loading...</div>
      ) : (
        <ul>
          {users.map((user) => (
            <li key={user.id}>
              {user.name}, {user.age} years old
              <button onClick={() => handleDeleteUser(user.id)}>Delete</button>
           
            </li>
          ))}
        </ul>
      )}

      <h3>Add New User</h3>
      <form>
        <div>
          <label>
            ID:
            <input type="text" name="id" value={newUser.id} onChange={handleInputChange} />
          </label>
        </div>
        <div>
          <label>
            Name:
            <input type="text" name="name" value={newUser.name} onChange={handleInputChange} />
          </label>
        </div>
        <div>
          <label>
            Age:
            <input type="text" name="age" value={newUser.age} onChange={handleInputChange} />
          </label>
        </div>
        <button type="button" onClick={handleAddUser}>Add User</button>
      </form>
    </div>
  );
}

export default UserList;
