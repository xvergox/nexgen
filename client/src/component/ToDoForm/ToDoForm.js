import { useState } from 'react'
import '../../style/index.css'

function ToDoForm({ addTask }) {
    const [userInput, setUserInput] = useState('');
    const handleChange = (e) => {
        setUserInput(e.currentTarget.value)
    };
    const handleSubmit = (e) => {
        e.preventDefault()
        addTask(userInput)
        setUserInput("")
    }
    const handleKeyPress = (e) => {
        if (e.key === "Enter") {
            handleSubmit(e);
        }
    };
    return (
        <>
            <div className="form__wrap">
                <form onSubmit={handleSubmit} className="form__add">
                    <input
                        className="form__add_input"
                        value={userInput}
                        type="text"
                        onChange={handleChange}
                        onKeyDown={handleKeyPress}
                        placeholder="Тыц..."
                    />
                    <button className="btn__add__save">Create Tack</button>
                </form>
            </div>
        </>
    );
};

export default ToDoForm