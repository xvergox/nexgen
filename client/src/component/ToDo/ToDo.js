import React from "react";
import "../../style/index.css"
function ToDo({ todo, toggleTask, removeTask, text }) {
    return (
        <>
            <div className="todo__wrap">
                <div key={todo.id + todo.key} className="todo__wrap">
                    <label htmlFor={todo.id}>
                        <input
                            id={todo.id}
                            onClick={() => toggleTask(todo.id)}
                            className="complete">
                        </input>
                        <div
                            className={todo.complete ? "todo__complete" : "todo__act"}
                        >
                            <div className="tack__text" id="text"> {text}</div>
                        </div>
                    </label>
                </div>
                <div className="todo__btn__delete" onClick={() => removeTask(todo.id)}>
                    +
                </div>
            </div>
        </>
    );
};

export default ToDo;