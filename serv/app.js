const express = require("express");
const fs = require("fs");

const app = express();
const jsonParser = express.json();

// Пример настройки CORS на сервере
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*'); // Здесь можно указать конкретные домены, с которых разрешен доступ
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    next();
  });
  

const filePath = "users.json";
// получение всех пользователей
app.get("/api/users", function (req, res) {

    const content = fs.readFileSync(filePath, "utf8");
   
    const users = JSON.parse(content);
    // console.log(users)
    res.status(200).send(users);
});
// получение одного пользователя по id
app.get("/api/users/:id", function (req, res) {

    const id = req.params.id; // получаем id
    const content = fs.readFileSync(filePath, "utf8");
    const users = JSON.parse(content);
    let user = null;
    // находим в массиве пользователя по id
    for (var i = 0; i < users.length; i++) {
        if (users[i].id == id) {
            user = users[i];
            break;
        }
    }
    // отправляем пользователя
    if (user) {
        res.send(user);
    }
    else {
        res.status(404).send();
    }
});
// получение отправленных данных
app.post("/api/users", jsonParser, function (req, res) {

    if (!req.body) return res.sendStatus(400);

    const userName = req.body.name;
    const userAge = req.body.age;
    let user = { name: userName, age: userAge };

    let data = fs.readFileSync(filePath, "utf8");
    let users = JSON.parse(data);

    // находим максимальный id
    const id = Math.max.apply(Math, users.map(function (o) { return o.id; }))
    // увеличиваем его на единицу
    user.id = id + 1;
    // добавляем пользователя в массив
    users.push(user);
    data = JSON.stringify(users);
    // перезаписываем файл с новыми данными
    fs.writeFileSync("users.json", data);
    res.send(user);
});
// удаление пользователя по id
app.delete("/api/users/:id", function (req, res) {

    const id = req.params.id;
    let data = fs.readFileSync(filePath, "utf8");
    let users = JSON.parse(data);
    let index = -1;
    // находим индекс пользователя в массиве
    for (var i = 0; i < users.length; i++) {
        if (users[i].id == id) {
            index = i;
            break;
        }
    }
    if (index > -1) {
        // удаляем пользователя из массива по индексу
        const user = users.splice(index, 1)[0];
        data = JSON.stringify(users);
        fs.writeFileSync("users.json", data);
        // отправляем удаленного пользователя
        res.send(user);
    }
    else {
        res.status(404).send();
    }
});
// изменение пользователя
app.put("/api/users", jsonParser, function (req, res) {

    if (!req.body) return res.sendStatus(400);

    const userId = req.body.id;
    const userName = req.body.name;
    const userAge = req.body.age;

    let data = fs.readFileSync(filePath, "utf8");
    const users = JSON.parse(data);
    let user;
    for (var i = 0; i < users.length; i++) {
        if (users[i].id == userId) {
            user = users[i];
            break;
        }
    }
    // изменяем данные у пользователя
    if (user) {
        user.age = userAge;
        user.name = userName;
        data = JSON.stringify(users);
        fs.writeFileSync("users.json", data);
        res.send(user);
    }
    else {
        res.status(404).send(user);
    }
});

// изменение пользователя
app.post("/api/users:add", jsonParser, function (req, res) {
    if (!req.body) return res.sendStatus(400);

    const userId = req.body.id;
    const userName = req.body.name;
    const userAge = req.body.age;
    console.log(req.body)
    let data = fs.readFileSync(filePath, "utf8");
    const users = JSON.parse(data);

    // Проверяем, что пользователь с данным ID не существует
    const userExists = users.some(user => user.id === userId);
    if (userExists) {
        return res.status(409).send("User with the same ID already exists.");
    }

    // Создаем нового пользователя
    const newUser = {
        id: userId,
        name: userName,
        age: userAge,
    };
    users.push(newUser);

    // Обновляем данные
    data = JSON.stringify(users);
    fs.writeFileSync("users.json", data);

    res.send(newUser);
});

app.listen(29, function () {
    console.log("  http://localhost:29/api/users");
});